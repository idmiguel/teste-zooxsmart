import { shallowMount } from "@vue/test-utils"
import index from "@/pages/index"  

describe('index', () => {

  test('is a Vue instance', () => {
    const wrapper = shallowMount(index)   
    expect(wrapper.isVueInstance()).toBeTruthy()
    expect(wrapper.html()).toMatchSnapshot()
  })
})