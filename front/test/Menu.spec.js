import { createLocalVue, shallowMount } from "@vue/test-utils"
import VueRouter from 'vue-router'
import menu from "@/components/parts/menu"
import index from "@/pages/index"
import readme from "@/pages/readme" 

describe('menu', () => {

  test('is a Vue instance', () => {
    const localVue = createLocalVue()
    localVue.use(VueRouter)
    const routes = [
    {
      path: '/',
      name: 'index',
      component: index
    },
    {
      path: '/readme',
      name: 'readme',
      component: readme
    }
    ]
    const router = new VueRouter({
      routes
    })
    const wrapper = shallowMount(menu, { data() { return { testJest: false }}, localVue, router })
    expect(wrapper.vm.$route).toBeInstanceOf(Object)
    expect(wrapper.isVueInstance()).toBeTruthy() 
    expect(wrapper.html()).toMatchSnapshot()
  })
})
