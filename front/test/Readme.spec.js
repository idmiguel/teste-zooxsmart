import { shallowMount } from "@vue/test-utils"
import readme from "@/pages/readme"  

describe('readme', () => {

  test('is a Vue instance', () => {
    const wrapper = shallowMount(readme)
    expect(wrapper.contains('div')).toBe(true)
    expect(wrapper.isVueInstance()).toBeTruthy() 
    expect(wrapper.html()).toMatchSnapshot()
  })
})