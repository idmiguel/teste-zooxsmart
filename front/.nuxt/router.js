import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _0304a367 = () => interopDefault(import('../pages/readme.vue' /* webpackChunkName: "pages/readme" */))
const _624d8910 = () => interopDefault(import('../pages/update/_estado.vue' /* webpackChunkName: "pages/update/_estado" */))
const _2c09a36a = () => interopDefault(import('../pages/update/_cidade/_cidade.vue' /* webpackChunkName: "pages/update/_cidade/_cidade" */))
const _d4b187ae = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/readme",
    component: _0304a367,
    name: "readme"
  }, {
    path: "/update/:estado?",
    component: _624d8910,
    name: "update-estado"
  }, {
    path: "/update/:cidade?/:cidade?",
    component: _2c09a36a,
    name: "update-cidade-cidade"
  }, {
    path: "/",
    component: _d4b187ae,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
