# test-zoox

> Teste Zoox API Rest - Cities and Towns

## First step: Build Setup Front-End

``` bash
# enter on folder
$ cd front/

# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# serve API at localhost:3001
# cd ../back
$ node index.js

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

## Second step: Build Setup Back-End

``` bash
# enter on folder
$ cd back/

# install dependencies
$ npm install
```

## Third step: run general application

### Go to the root folder

``` bash
# run project
$ make start
 
```